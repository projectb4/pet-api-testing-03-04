import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();

describe("Delete current user by ID", () => {
    let accessToken: string;
    let userId: number;

    before(`Login and get the token`, async () => {
        let response = await auth.login("MaryTeresa@gmail.com", "Mtaer59");

        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;
        //console.log(accessToken);
        
    });

    it('Should delete the user by ID', async () => {
        let response = await users.deleteUserById(userId, accessToken);
        expect(response.statusCode, 'Status code is 204').to.be.equal(204);
    });

    it('Should not found deleted user', async () => {
        let response = await users.getUserById(userId);
        expect(response.statusCode, 'Status code is 404').to.be.equal(404);
    });
});

