import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();
const chai = require('chai');
chai.use(require('chai-json-schema'));


describe("Get current user information by ID", () => {
    it('Should return correct details of current user', async () => {
        let response = await users.getUserById(5832);
        expect(response.statusCode, 'Status code is 200').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time is less than 1s').to.be.lessThan(1000);
        expect(response.body.id, 'User ID is correct').to.equal(333);
        expect(response.body.avatar, 'User avatar is correct').to.equal("string");
        expect(response.body.email, 'User email is correct').to.equal("MaryTeresa@gmail.com");
        expect(response.body.userName, 'User name is correct').to.equal("MaryTeresa");
    });
});

