import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));


describe("Get details of the current logged in user", () => {
    let accessToken: string;
    let userId: number;
    let userAvatar: string = "";
    let userEmail: string = "MaryTeresa3927@gmail.com";
    let userName: string = "MaryTeresa";
    let userPassword: string = "Mtaer59";


    before(`Login and get the token`, async () => {
        let response = await auth.login("MaryTeresa3927@gmail.com", "Mtaer59");
 
        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;
        // console.log(accessToken);
    });

    it("Should return correct details of current user", async () => {
        let response = await users.getCurrentUser(accessToken);
       
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
    });
});
