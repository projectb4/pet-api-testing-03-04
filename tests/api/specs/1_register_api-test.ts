import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { RegisterController } from "../lib/controllers/register.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
	
const register = new RegisterController();

describe("Registration", () => {
it("should register a new user successufully", async () => {
    const newUser = {
	        id: 333,
	        avatar: "string",
            email: "MaryTeresa3927@gmail.com",
            userName: "MaryTeresa",
            password:"MTaer59"
	    };
	
	    const response = await register.User(newUser);
        
        console.log("Response status code:", response.statusCode);
        console.log("Response body:", response.body);
         
        checkStatusCode(response,200);


	});
});

