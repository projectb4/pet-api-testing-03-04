import { checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
const auth = new AuthController();

describe("Without test data set for login - bad example", () => {
    it(`should not login using invalid credentials email: 'Ma.ry.Teresa@gmail.com', password: ''`, async () => {
        let response = await auth.login("Ma.ry.Teresa@gmail.com", "");

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });

    it(`should not login using invalid credentials email: '@gmail.com', password: '      '`, async () => {
        let response = await auth.login("@gmail.com", "      ");

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });

    it(`should not login using invalid credentials email: 'Ma.ry.Teresa@.com', password: '  Mtaer59'`, async () => {
        let response = await auth.login("Ma.ry.Teresa@.com", "  Mtaer59");

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });

    it(`should not login using invalid credentials email: 'Ma.ry.Teresagmail.com', password: 'Mtaer59'`, async () => {
        let response = await auth.login("Ma.ry.Teresagmail.com", "Mtaer59");

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });

    it(`should not login using invalid credentials email: 'Ma.ry.Teresa@gmail.com', password: 'Ma.ry.Teresa@gmail.com'`, async () => {
        let response = await auth.login("Ma.ry.Teresa@gmail.com", "Ma.ry.Teresa@gmail.com");

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });
});
