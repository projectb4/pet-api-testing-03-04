import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();

describe("Token usage", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("MaryTeresa3927@gmail.com", "Mtaer59");
 
        accessToken = response.body.token.accessToken.token;
        // console.log(accessToken);
    });

    it(`Usage is here`, async () => {
        let userData: object = {
            id:333,
            avatar: "string",
            email: "MaryTeresa3927@gmail.com",
            userName: "MaryTeresa",
        };

        let response = await users.updateUser(userData, accessToken);
        checkStatusCode(response, 200);
    });
});
