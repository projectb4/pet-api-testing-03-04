import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const users = new PostsController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe('Get list of all posts', () => {
 
    it('Should return all posts and response status code is 200', async () => {
        let response = await users.getAllPosts();

        expect(response.statusCode, 'Status code is 200').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time is less than 1s').to.be.lessThan(1000);
        expect(response.body.length, 'Response body has more than 1 item').to.be.greaterThan(1); 
        expect(response.body).to.be.jsonSchema(schemas.schema_allPosts);
    });
});

