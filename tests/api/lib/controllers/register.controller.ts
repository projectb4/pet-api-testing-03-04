import { ApiRequest } from "../request";

const baseUrl:string = "http://tasque.lol/";

export class RegisterController {
    [x: string]: any;
    async registerUser(id: 0, avatarValue: string, emailValue: string, usernameValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body({
                id: 0,
                avatar: avatarValue,
                email: emailValue,
                username: usernameValue,
                password: passwordValue,
            })
            .send();
        return response;
    }
}
