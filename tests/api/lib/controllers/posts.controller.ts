import { ApiRequest } from "../request";

const baseUrl:string = "http://tasque.lol/";

export class PostsController{
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

    async createNewPost(authorIdValue: number, previewImageValue: string, bodyValue: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url('/api/Posts')
            .body({
                authorId: authorIdValue,
                previewImage: previewImageValue,
                body: bodyValue
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
    async addLikeToPost(entityIdValue, isLikeValue: boolean, userIdValue: number, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url('/api/Posts/like')
            .body({
                entityId: entityIdValue,
                isLike: isLikeValue,
                userId: userIdValue
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
}


